﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public enum CellType { Normal, Empty }

    public int Value { get; set; }
    //public int Index { get; set; }
    public CellType Type { get; set; }
    public Vector3 Position { get; set; }


    public CellManager CellManager { get; set; }

    Vector3 downPos;
    bool canMove;

    void Start()
    {
        if (Type == CellType.Empty)
        {
            GetComponentInChildren<Image>().enabled = false;
            GetComponentInChildren<TextMeshProUGUI>().enabled = false;
        }
        else
        {
            GetComponentInChildren<TextMeshProUGUI>().text = $"{Value}";
        }

        Invoke("SetPosition", 0.1f);

        CellManager.SubscribeOnStartGame((t) => canMove = true);
        CellManager.SubscribeOnEndGame(() => canMove = false);
    }
    void SetPosition()
    {
        Position = transform.position;

    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if(canMove) CellManager.StartMove(this, Input.mousePosition);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (canMove) CellManager.EndMove(this, Input.mousePosition);
    }

    public void Move(Vector3 pos)
    {
        StartCoroutine(CoMove(pos));
    }

    IEnumerator CoMove(Vector3 pos, float animTime = 0.1f)
    {
        Position = pos;

        if (Type == CellType.Normal)
        {
            Vector3 startPos = transform.position;
            float t = 0;
            while(t <= animTime)
            {
                transform.position = Vector3.Lerp(startPos, pos, t/animTime);
                t += Time.deltaTime;
                yield return null;
            }
        }

        transform.position = pos;
    }
}
