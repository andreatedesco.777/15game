﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIHandler : MonoBehaviour
{
    CellManager cellManager;

    [Header("PANELS")]
    [SerializeField] GameObject endPanel;
    [SerializeField] GameObject pausePanel;

    [Header("GAME PANEL")]
    [SerializeField] TextMeshProUGUI movesText;
    [SerializeField] TextMeshProUGUI timeText;

    [Header("END PANEL")]
    [SerializeField] TextMeshProUGUI finalMovesText;
    [SerializeField] TextMeshProUGUI finalTimeText;

    bool start;
    float startTime;

    private void Start()
    {
        cellManager = CellManager.Instance;
        cellManager.SubscribeOnStartGame((startTime) => { start = true; this.startTime = startTime; } );
        cellManager.SubscribeOnEndGame(EndGame);
        cellManager.SubscribeOnChangeCounterMoves((value) => movesText.text = $"{value} Moves");
    }
    private void Update()
    {
        if (start) timeText.text = $"{(Time.time - startTime).ToString("f0")} Seconds";
    }
    public void OnStart(int difficulty)
    {
        cellManager.OnStart(difficulty);
    }

    public void OnReload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnPause()
    {
        pausePanel.SetActive(!pausePanel.activeSelf);
        Time.timeScale = (Time.timeScale + 1) % 2;
    }

    void EndGame()
    {
        start = false;
        endPanel.SetActive(true);

        finalMovesText.text = movesText.text;
        finalTimeText.text = timeText.text;
    }
}
