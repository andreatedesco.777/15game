﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp : MonoBehaviour
{
    [SerializeField] float timeToZoom = 3f;

    private IEnumerator CPopUp(bool zoomOn)
    {

        Vector3 finalScale = Vector3.one;
        Vector3 initialScale = Vector3.zero;

        float t = 0;
        while (t < timeToZoom)
        {
            transform.localScale = Vector3.Slerp(initialScale, finalScale, t / timeToZoom);
            t += Time.deltaTime;
            yield return null;
        }
        transform.localScale = finalScale;

    }

    void OnEnable()
    {
        StartCoroutine(CPopUp(true));
    }

}
