﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellManager : MonoBehaviour
{
    #region Events

    Action<float> OnStartGame;
    Action OnEndGame;
    Action<int> OnChangeCounterMoves;

    public void SubscribeOnStartGame(Action<float> a)
    {
        OnStartGame += a;
    }

    public void SubscribeOnEndGame(Action a)
    {
        OnEndGame += a;
    }    
    
    public void SubscribeOnChangeCounterMoves(Action<int> a)
    {
        OnChangeCounterMoves += a;
    }

    #endregion

    #region Variables

    int dimension = 16;
    public enum MoveDirection { Up = -4, Down = +4, Rigth = +1, Left = -1 }
    public enum Difficulty { Easy = 100, Medium = 300, Hard = 1000 }

    public static CellManager Instance {get; set;}

    //Board Management
    Cell[] board = new Cell[16];
    Cell emptyCell;
    int emptyIndex; // 0 to 15

    //Board Creation
    [SerializeField] Cell prefabCell;
    [SerializeField] Transform cellContainer;

    // Input Detenction
    Cell currentCell;
    Vector3 startPos;
    List<int> indexMovable = new List<int>(); //indices of cells that can move

    [SerializeField] Difficulty difficulty = Difficulty.Easy;
    [SerializeField] float shuffleVelocity = 0.05f;
    int counterMoves;
    #endregion

    void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);

        Init();
    }

    void Init()
    {
        for (int i = 0; i < dimension; ++i)
        {
            var cell = Instantiate(prefabCell, cellContainer);
            cell.Value = i + 1;
            cell.CellManager = this;

            if (i == dimension - 1)
            {
                cell.Type = Cell.CellType.Empty;
                emptyCell = cell;
                emptyIndex = i;
            }

            board[i] = cell;
        }

    }

    /// <summary>
    /// Calculates the indices of the cells that can move starting from newIndex
    /// </summary>
    /// <param name="newIndex">Index of the empty cell</param>
    void LoadMovable(int newIndex = 15)
    {
        indexMovable.Clear();
        emptyIndex = newIndex;
        CheckMovableDirection(MoveDirection.Up);
        CheckMovableDirection(MoveDirection.Down);
        CheckMovableDirection(MoveDirection.Left);
        CheckMovableDirection(MoveDirection.Rigth);
    }

    void CheckMovableDirection(MoveDirection dir)
    {
        if (emptyIndex % 4 == 0 && dir == MoveDirection.Left || (emptyIndex+1) % 4 == 0 && dir == MoveDirection.Rigth) return;
        var index = emptyIndex + (int)dir;
        if (index >= 0 && index <= 15) indexMovable.Add(index);
    }

    public void StartMove(Cell cell, Vector3 startPosition)
    {
        currentCell = cell;
        startPos = startPosition;
    }
    public bool EndMove(Cell cell, Vector3 endPosition)
    {
        var index = CellToIndex(cell);
        if (!indexMovable.Contains(index)) return false;
        Move(currentCell, index);
        return true;
    }

    public void OnStart(int difficulty = 0)
    {
        switch (difficulty)
        {
            case 1:
                this.difficulty = Difficulty.Medium;
                break;          
            case 2:
                this.difficulty = Difficulty.Hard;
                break;
            default:
                this.difficulty = Difficulty.Easy;
                break;
        }
        StartCoroutine(CoStart());
    }

    IEnumerator CoStart()
    {
        LoadMovable();
        yield return StartCoroutine(CoShuffle());

        OnStartGame?.Invoke(Time.time);
    }

    IEnumerator CoShuffle()
    {
        int random = UnityEngine.Random.Range((int)difficulty / 2, (int)difficulty);
        for (int i = 0; i < random; ++i)
        {
            int randomIndex = UnityEngine.Random.Range(0, indexMovable.Count);
            int currentIndex = indexMovable[randomIndex];
            Cell currentCell = IndexToCell(currentIndex);
            Move(currentCell, currentIndex, true);
            yield return new WaitForSeconds(shuffleVelocity);
        }
    }

    void Move(Cell currentCell, int currentIndex, bool shuffle = false)
    {
        var currentPos = currentCell.Position;

        currentCell.Move(emptyCell.Position);
        board[emptyIndex] = currentCell;

        emptyCell.Move(currentPos);
        board[currentIndex] = emptyCell;

        LoadMovable(currentIndex);

        if (!shuffle)
        {
            if (CheckWin()) OnEndGame?.Invoke();
            OnChangeCounterMoves?.Invoke(++counterMoves);
        }
    }

    //It could have been avoided by putting the index information in the cell, but it would have had too many responsibilities
    int CellToIndex(Cell cell)
    {
        for (int i = 0; i < dimension; ++i) if (cell == board[i]) return i;
        return -1;
    }

    Cell IndexToCell(int index)
    {
        return board[index];
    }

    bool CheckWin()
    {
        for (int i = 0; i < dimension; ++i) if (i+1 != board[i].Value) return false;
        return true;
    }
}
